---
title: about
date: 2020-10-31 20:52:15
layout: about
---

# Hexo

- 安装Hexo

```bash
npm i hexo-cli -g
```

- 初始化博客

```bash
hexo init blog
cd blog
npm install
hexo server
```

# Fluid主题: [GitHub](https://github.com/fluid-dev/hexo-theme-fluid/)

- 安装主题

在博客主目录下，执行下面的命令：

```bash
npm install --save hexo-theme-fluid
```

- 配置

下载配置[_config.yml_](https://raw.githubusercontent.com/fluid-dev/hexo-theme-fluid/release/_config.yml)，将之更名为`_config.fluid.yml`，并修改该文件中的配置：

```yaml
theme: fluid
language: zh-CN
```

- 创建博客的关于页：

```bash
hexo new page about
```

- 更新主题：

```bash
npm update --save hexo-theme-fluid
```

