---
uuid: 4e15caf0-7349-11eb-bce0-c1c051abe1ef
title: zsh
date: 2021-02-20 15:00:24
categories: 
    - Linux
    - Shell
tags:
    - Linux
    - Shell
---

## 1. 安装Zsh

```bash
sudo pacman -S zsh
```


## 2. 安装oh-my-zsh

* 配置代理

```bash
# 设置代理
export http_proxy="http://127.0.0.1:1081"
export https_proxy="http://127.0.0.1:1081"
# 自定义配置目录
ZSH="$HOME/.config/oh-my-zsh"
mkdir -p $ZSH
# 安装oh-my-zsh(需Git)
sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
```

## 3. 常用插件

* 内置: `extract`, `sudo`

* 其它

```bash
# zsh-autosuggestions:
git clone https://github.com/zsh-users/zsh-autosuggestions $ZSH_CUSTOM/plugins/zsh-autosuggestions
# zsh-syntax-highlighting:
git clone https://github.com/zsh-users/zsh-syntax-highlighting.git $ZSH_CUSTOM/plugins/zsh-syntax-highlighting
```
