---
uuid: 74cb3cf0-734b-11eb-8e15-d7aa06181018
title: qv2ray
date: 2021-02-20 15:15:48
categories:
	- Network
tags:
	- Network
---

## Qv2ray安装与配置

### 安装V2Ray

```bash
sudo pacman -S v2ray
```

### 安装Qv2ray

1、启用multilib

```bash
# 打开源配置文件
vim /etc/pacman.conf
# 取消下面两行的注释：
[multilib]
Include = /etc/pacman.d/mirrorlist
```

2、配置archlinuxcn源

```bash
# 打开源配置文件
vim /etc/pacman.conf
# 在后面添加：
[archlinuxcn]
Server = https://mirrors.aliyun.com/archlinuxcn/$arch
```

3、安装yay

```bash
sudo pacman -S archlinuxcn-keyring
sudo pacman -S yay
sudo pacman -S archlinuxcn-mirrorlist-git
```

4、用yay安装Qv2ray

```bash
yay -S qv2ray
```