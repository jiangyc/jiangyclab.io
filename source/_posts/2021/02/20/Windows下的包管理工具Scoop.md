---
uuid: 03892e10-734d-11eb-bdf3-8d6661dfbca1
title: Windows下的包管理工具Scoop
date: 2021-02-20 15:26:57
categories:
	- Windows
tags:
    - Windows
    - 包管理
---

## 简介

[https://scoop.sh](https://scoop.sh)：一个Windows上的包管理工具。

**示例：**

要安装curl，直接执行`scoop install curl`即可。

## 安装

> 注意，要用`Powershell`来运行下列的命令。所以推荐去Windows应用商店搜索下载`Powershell`和`Windows Terminal`，并在`Windows Terminal`中，将下载的`Powershell`设置为默认。

* 首先，要允许当前用户执行第三方签名的脚本：

```powershell
Set-ExecutionPolicy RemoteSigned -scope CurrentUser
```

* 设置安装目录（可选）

```powershell
# 用户目录：Scoop安装目录，默认为~/scoop。如不用"-g"参数特殊指定全局安装，Scoop安装的所有程序及数据都会在该目录中
$env:SCOOP='path\to\scoop\userdir'
[Environment]::SetEnvironmentVariable('SCOOP', $env:SCOOP, 'User')
# 全局目录：用"-g"参数指定全局安装的程序所在目录。默认为系统盘的ProgramData目录。
$env:SCOOP_GLOBAL='path\to\scoop\globaldir'
[Environment]::SetEnvironmentVariable('SCOOP_GLOBAL', $env:SCOOP_GLOBAL, 'Machine')
```

* 安装Scoop

```powershell
Invoke-Expression (New-Object System.Net.WebClient).DownloadString('https://get.scoop.sh')
# 或则
iwr -useb get.scoop.sh | iex
```

## 其它

### Bucket

使用`scoop bucket known`可插件scoop当前可启用的所有Bucket(相当于Ubuntu的PPA)。默认只启用了`main`，推荐将`extras`、`nerd-fonts`、`java`、`jetbrains`启用。启用命令如下：

```powershell
scoop bucket add <仓库名称> [仓库地址]
```

针对自带的这几个仓库，可省略仓库地址。如要添加第三方的Bucket，则仓库名称和仓库地址都不能忽略。

### 推荐

一些推荐安装的应用：

* git：该应用依赖于7zip
* neovim
* python
* sudo
* typora
* v2ray
* nvm
* motrix
* opera
* vivaldi
* putty
* ventoy
* dbeaver
* Cascadia-Code：`Cascadia Code`字体
* CascadiaCode-NF-Mono：Nerd字体
* FiraCode-NF-Mono：Nerd字体
