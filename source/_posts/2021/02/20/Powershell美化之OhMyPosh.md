---
uuid: e6a39ec0-7351-11eb-bebe-25225645833b
title: Powershell美化之OhMyPosh
date: 2021-02-20 16:01:56
categories: 
	- Windows
	- Powershell
tags:
	- Windows
---

> 该主题可能需要Nerd字体，推荐用`scoop`安装。推荐去Windows应用商店安装`Windows Terminal`与最新版的`Powershell`，能获得更好的体验。

## 安装

```powershell
# 允许当前用户执行第三方签名的脚本
Set-ExecutionPolicy RemoteSigned -scope CurrentUser
# 安装
Install-Module oh-my-posh -Scope CurrentUser -AllowPrerelease
# 列出可用主题
Get-PoshThemes
# 临时设置主题（将该命令执行，并写入在$profile文件中即可永久保存）
Set-PoshPrompt -Theme agnoster
```
