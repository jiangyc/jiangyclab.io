---
uuid: b8548c30-1b7e-11eb-a2e2-fdef40154b8a
title: Archlinux
date: 2020-10-31 21:41:03
index_img: /img/archlinux.png
categories: 
	- Linux
	- Archlinux
tags:
	- Linux
	- Archlinux
---

>  **A simple, lightweight distribution**
>
> You've reached the website for **Arch Linux**, a lightweight and flexible Linux® distribution that tries to Keep It Simple.
>
> Currently we have official packages optimized for the x86-64 architecture. We complement our official package sets with a [community-operated package repository](https://aur.archlinux.org/) that grows in size and quality each and every day.
>
> Our strong community is diverse and helpful, and we pride ourselves on the range of skillsets and uses for Arch that stem from it. Please check out our [forums](https://bbs.archlinux.org/) and [mailing lists](https://mailman.archlinux.org/mailman/listinfo/) to get your feet wet. Also glance through our [wiki](https://wiki.archlinux.org/) if you want to learn more about Arch.

<!-- more -->

# 一、下载镜像

你可以从下列的地址中，选择合适的一个来下载系统安装镜像

* [官方地址](https://www.archlinux.org/download/)
* [中国科学技术大学开源软件镜像](http://mirrors.ustc.edu.cn/archlinux/iso/latest/)
* [清华大学开源软件镜像站](https://mirrors.tuna.tsinghua.edu.cn/)]

# 二、验证签名

在一台已经安装 [GnuPG](https://wiki.archlinux.org/index.php/GnuPG) 的系统上，可通过下载 *PGP 签名* (在 [下载](https://www.archlinux.org/download/) 页面的 *Checksums* 下方) 到 ISO 文件所在的路径，然后用以下方式 [验证](https://wiki.archlinux.org/index.php/GnuPG#Verify_a_signature)：

```bash
gpg --keyserver-options auto-key-retrieve --verify archlinux-version-x86_64.iso.sig
```

另外，在一台已经安装 Arch Linux 的计算机上可以通过以下方式验证：

```bash
pacman-key -v archlinux-version-x86_64.iso.sig
```

# 三、准备映像

将ISO镜像刻录到U盘等设备，可考虑使用下列的工具：

* [Rufus](rufus.ie/)

一个简单但可靠的镜像刻录工具。

* [Ventoy](https://github.com/ventoy/Ventoy)

可允许多个镜像共存的启动工具。

# 四、安装准备

在BIOS中，设置U盘启动，并启动到安装环境，准备开始安装系统。

## 1、网络设置

如果你使用网线，应该能直接连接到网络。使用下列的命令来检测联网：

```bash
ping www.baidu.com
```

如果你需要连接到WiFI，则可使用iwctl来帮助你：

```bash
# 在终端中输入iwctl进入iwd提示符
iwctl
# 在[iwd#]中输入device list查询机器的网卡设备
device list
# 在[iwd#]中连接wifi网络，如果wifi加密，会提示你输入密码：示例：station wlan0 connect xxxx
station devicename connect wifi-ssid  
```

## 2、时间同步

使用timedatectl命令来确保时间是同步的：

```bash
timedatectl set-ntp true
#确保设置成功
timedatectl status
```

# 五、磁盘分区

## 1、分区说明

首先使用lsblk或者其他磁盘工具（例如fdisk）查看磁盘设备：

```bash
lsblk
```

然后使用fdisk或者cfdisk创建磁盘分区。在创建分区的时候， 必需要确保有一个root分区/ 。对于以UEFI模式引导的还需要一个EFI系统分区。假设现在的硬盘为`/dev/sda`，可将其按如下数据进行分区：

| 挂载点    |                 分区类型              | 大小 | 设备 |
|:---------|:-------------------------------------|----------|----------|
| /boot    |      fat32                       | 250M | /dev/sda1 |
| ---- |      swap                    | 内存的一半 | /dev/sda2 |
| / | ext4 | 剩余 | /dev/sda3 |

## 2、格式分区

分区后，要先格式化才能使用，例如：

```bash
# /boot分区
mkfs.fat -F32 /dev/sda1
# swap分区
mkswap /dev/sda2
swapon /dev/sda2
# /分区
mkfs.ext4 /dev/sda3
```

# 六、安装系统

安装基本的系统

## 1、挂载分区

使用`mount`命令来挂载分区：

```bash
mount /dev/sda3 /mnt
mkdir /mnt/boot
mount /dev/sda1 /mnt/boot
```

## 2、安装系统

使用`pacstrap`命令来安装基本的系统：

```bash
pacstrap /mnt base base-devel linux linux-firmware
```

生成`fstab`文件：

```bash
genfstab -U /mnt >> /mnt/etc/fstab
```

## 3、安装引导

首先，使用`arch-chroot /mnt`命令，来切换到基本系统环境中，执行命令：

```bash
# 刷新软件包缓存
pacman -Syyu
# 安装grub包
pacman -S grub
# grub安装
grub-install --target=x86_64-efi --efi-directory=/boot --bootloader-id=Arch
grub-mkconfig -o /boot/grub/grub.cfg
```

引导安装成功后，就可以正确的重启进入系统了。

# 七、后续配置

为了更好的使用，来需要进行一些基本的配置：

- root密码修改

```bash
passwd
```

* 必备包

```bash
pacman -S dhcpcd networkmanager vim sshd sudo
systemctl enable dhcpcd
systemctl enable NetworkManager
systemctl enable sshd
```

- 启用`wheel`组：

编辑`/etc/sudoers`文件，

```bash
vim /etc/sudoers
```

取消对应行前面的"#"

- 编码配置

```bash
vim /etc/locale.gen
```

一般来说，需要启用`en_US.UTF-8`、`zh_CN.UTF-8`、`zh_HK.UTF-8`、`zh_TW.UTF-8`、`zh_SG.UTF-8`等编码

然后，使用如下的命令来重新生成编码：

```
locale-gen
```

- 时区设置

```bash
ln -sf /usr/share/zoneinfo/Asia/Shanghai /etc/localtime
hwclock --systohc
timedatectl set-ntp true
```

- 主机名称

编辑`/etc/hostname`文件，写入对应的主机名称即可

- hosts配置

编辑`/etc/hosts`文件，写入以下配置：

```bash
127.0.0.1 localhost
::1       localhost
127.0.1.1 主机名.localdomain 主机名
```

