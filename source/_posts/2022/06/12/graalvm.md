---
uuid: 4df24ba0-ea0a-11ec-9d22-b5e45f72a443
title: graalvm
date: 2022-06-12 12:44:11
categories:
	- Java
	- Graalvm
tags:
	- Java
	- Graalvm
---

官网：[https://www.graalvm.org](https://www.graalvm.org)

GitHub：[https://github.com/oracle/graal/](https://github.com/oracle/graal/)

# 安装

GraalVM分为一个单独的基于OpenJDK的环境，和各个组件。在安装了GraalVM后，还要使用它提供的`gu`命令来安装组件。

## GraalVM

- Windows

从[官网](https://www.graalvm.org/downloads/)或从[GitHub](https://github.com/graalvm/graalvm-ce-builds/releases)下载均可。

- Linux

从各大发行商应用仓库下载即可，以ArchLinux为例，从AUR下载：

```bash
paru -S jdk17-graalvm-bin
```

## 组件

GraalVM提供了gu命令来管理各个组件。在只安装了GraalVM后，只有一个`graalvm`组件。

可以用`gu list`来查看以安装的组件。

如果要查看当前的GraalVM支持那些组件，可以用`gu available`命令。

使用`gu install 要安装的组件`命令来安装组件。

比如，我们用之构建原生的可执行文件，只安装`native-image`和`llvm-toolchain`即可：

```bash
gu install native-image
gu install llvm-toolchain
```

# 使用

使用GraalVM提供的`native-image`来构建原生的可执行文件。它可以根据单独的类、jar或是jdk9的模块来构建。本文只介绍前两种。

它的用法是：

```bash
native-image [options] [-cp/-classpath <class search path of directories and zip/jar files>] class [imagename] [options]
```

## 单独的类

**PS：此种只适合写一个Hello World测试，真实用的话，还是要用下面的基于jar来构建的方法。**

首先，准备好我们的Hello World，一个叫`Hello.java`的文件，内容如下：

```java
public class Hello {

    public static void main(String[] args) {
        System.out.println("Hello World!");
    }
}
```

然后，编译并运行此类：

```bash
# 编译此类，生成Hello.class
javac Hello.java
# 运行
java Hello
```

有了`Hello.class`，我们就可以使用`native-image`来邮件原生的可执行程序了：

```bash
native-image Hello hello
```

生成的文件有两个：`hello`和`hello.build_artifacts.txt`。其中，`hello`即是我们的最终产物，大小大约为12M。

**PS：构建大概需要几分钟的时间，并要求电脑至少配有5G的内存。如果你的HelloWorld需要网络连接的话，要在构建命令中加上 --enable-http和--enable-https的选项。加上此选项后，最终可执行文件的大小可能会突破40M。**

## 一个或多个jar

分析：

```bash
java -agentlib:native-image-agent=config-output-dir=./META-INF/native-image -cp $CP MainClass
```

构建：

```bash
native-image --allow-incomplete-classpath -H:+ReportExceptionStackTraces -H:EnableURLProtocols=http,https --no-fallback -cp $CP  MainClass imagename
# --report-unsupported-elements-at-runtime
# --enable-url-protocols=css
```

